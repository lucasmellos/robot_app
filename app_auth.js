var panel = require('./build/Release/panel.node');
var express = require('express');
var app = express();

var server = require('http').createServer(app);
var io = require('socket.io')(server);
var Auth = require('./auth.js');

// usar arquivo de configuracao
var secret = '2594ew408eea5eb32a17a0f4626d4f21';
var port = 80;
var ssi_address = 'relle.ufsc.br:8080';
var lab_id = 12;

server.listen(port, function () {
    console.log('Server listening at port %d', port);
});

var configured = false;

io.on('connection', function (socket) {
    var auth = new Auth(ssi_address, secret, lab_id)

    var posicao = 1;
    socket.on('new connection', function (data) {
        console.log('new connection ', data, new Date());

        if (typeof (data.pass) === 'undefined') {
            socket.emit('err', {code: 402, message: 'Missing authentication token.'});
            console.log('erro 402');
            return;
        }

        var ev = auth.Authorize(data.pass);

        ev.on("not authorized", function () {
            socket.emit('err', {code: 403, message: 'Permission denied. Note: Resource is using external scheduling system.'});
            console.log('not authorized');
            return;
        })

        ev.on("authorized", function () {

            posicao = 1;
            var res = {};
            if (panel.run()) {
                panel.update(posicao);
                panel.digital(1);
                res.pos = posicao - 1;
                socket.emit('initial', res);
                panel.exit();
            }

        })

    });

    socket.on('new message', function (data) {

        if (!auth.isAuthorized()) {
            socket.emit('err', {code: 403, message: 'Permission denied. Note: Resource is using external scheduling system.'});
            console.log('erro 403');
            return;
        }

        var res = {};
        console.log('new message :' + data);
        if (panel.run()) {
            var newpos;
            if (data.key == "right") {
                newpos = ((posicao + 1) % 5 == 0) ? 1 : posicao + 1;
            } else {
                newpos = ((posicao - 1) % 5 == 0) ? 4 : posicao - 1;
            }
            console.log("pos: " + newpos);
            panel.update(newpos);
            res.pos = newpos - 1;
            socket.emit('initial', res);
            posicao = newpos;
            panel.exit();
        }

    });

    socket.on('disconnect', function () {
        
        if (!auth.isAuthorized()) {
            socket.emit('err', {code: 403, message: 'Permission denied. Note: Resource is using external scheduling system.'});
            console.log('erro 403');
            return;
        }
        
        if (panel.run()) {
            panel.update(1);
            panel.digital(0);
            panel.exit();
        }
        
        configured = false;
        console.log('disconnected');
    });


});

